/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Modelo;

/**
 *
 * @author Hugaleno
 */
public class Mensagem {
    private final int id;
    private final String msg;
    private final char[] mensagem;

    public Mensagem(int id, String msg) {
        this.id = id;
        this.msg = msg;
        this.mensagem = msg.toCharArray();
        
    }
    
    public int getId() {
        return id;
    }
    
    public String getDaPosicao(int posicao){
        if(posicao >= getTamanhoMensagem()){
            return null;
        }
        return mensagem[posicao]+"";
    }
    
    public String getDaPrimeiraPosicao(){
        return mensagem[0]+"";
    }
    
    public int getTamanhoMensagem(){
        return mensagem.length;
    }

    @Override
    public String toString() {
       return this.msg;
    }
    
    
    
}
