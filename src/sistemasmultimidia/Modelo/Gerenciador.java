/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Modelo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import sistemasmultimidia.Controle.Controller;
import sistemasmultimidia.SistemasMultimidia;

/**
 *
 * @author Hugaleno
 */
public class Gerenciador {
    
    private String seq = null;

    /**
     * @param path Recebe uma string contendo o caminho do arquivo selecionado 
     * @return Retorna um arraylist contendo todos os simbolos ou nulo caso o arquivo esteja vazio.
     */
    public ArrayList<Simbol> lerArquivo(String path) {
        Scanner sc = null;
        try {
            sc = new Scanner(new FileReader(path));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SistemasMultimidia.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (sc != null && sc.hasNext()) {
            seq = sc.nextLine();
            return separaSimbolos(seq);
        }

        return null;
    }
    
    /**
     *
     * @param array Um array de simbolos.
     * @return O valor da entropia calculada.
     */
    public double calculaEntropia(ArrayList<Simbol> array) {
        double H = 0, aux;
        for (Simbol simbol : array) {
            aux = Math.log10(simbol.getFrequencia()) / Math.log10(2);
            H += simbol.getFrequencia() * aux;
        }
        return H * (-1);
    }

     /**
     *
     * @param seq Sequencia de simbolos extraídas do arquivo.
     * @return Um Arraylist com todos os objetos criados a partir de cada símbolo da sequência.
     */
    
    
    private ArrayList<Simbol> separaSimbolos(String seq) {
        char[] vetorChar = seq.toCharArray();
        int cont = 0;
        HashSet<String> set = new HashSet<>();
        ArrayList<Simbol> listaDeSimbolos = new ArrayList<>();
        for (int i = 0; i < vetorChar.length; i++) {
            if (set.add(vetorChar[i] + "")) {
                listaDeSimbolos.add(new Simbol(vetorChar[i] + ""));
            }
        }
        for (Simbol simbolo : listaDeSimbolos) {

            for (int i = 0; i < seq.length(); i++) {

                if (simbolo.getNome().equals(seq.charAt(i) + "")) {
                    cont++;
                }
            }
            simbolo.setOcorrencias(cont);
            simbolo.setFrequencia(cont / (double) seq.length());
            cont = 0;
        }
        return listaDeSimbolos;
    }
    
    public Mensagem getMensagem(){
       StringBuilder adicionaCaracterFinal = new StringBuilder(seq);
       adicionaCaracterFinal.append('§');
       return (new Mensagem(++Controller.ID_MSG, adicionaCaracterFinal.toString()));
    }
}
