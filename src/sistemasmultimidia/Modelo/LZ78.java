/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Modelo;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author Hugaleno
 */
public class LZ78 {

    private final Mensagem mensagem;
    private final HashMap<String, Integer> mMapSequenciasUnicas;
    private String[] mDicionario;
    private ArrayList<String> listaSeqUnicas;
    private ArrayList<String> listaCaracteresUnicos;
    private final String caracterDeEscape = "¬";

    
    public LZ78(Mensagem mensagem) {
        this.mensagem = mensagem;
        mMapSequenciasUnicas = new HashMap<>();
    }
    
    
    public String getCaracterDeEscape() {
        return caracterDeEscape;
    }

    /**
     *
     *@return Retorna o número de bits que serão necessários para representar o maior número presente no dicionário.
     */
    
    public int getNumeroBits() {
        int chaveMaior = 0;
        for (String md : mDicionario) {
            String subString = md.substring(0, md.length() - 2);
            
            if (Integer.parseInt(subString) > chaveMaior) {
                chaveMaior = Integer.parseInt(subString);

            }
        }

        return (int) (Math.log10(chaveMaior) / Math.log10(2) + 1);
    }

    

    public String[] getmDicionario() {
        return mDicionario;
    }

    public Mensagem getMensagem() {
        return mensagem;
    }

    public ArrayList<String> getNumSequenciaUnicas() {
        listaSeqUnicas = new ArrayList<>();
        String[] aux;
        for (String md : mDicionario) {
            aux = md.split(caracterDeEscape);
            listaSeqUnicas.add(aux[0]);
        }
        return listaSeqUnicas;
    }

    public ArrayList<String> getCaracteres() {
        listaCaracteresUnicos = new ArrayList<>();
        for (String md : mDicionario) {
            listaCaracteresUnicos.add(md.charAt(md.length() - 1) + "");
        }
        return listaCaracteresUnicos;
    }

    /**
     *
     * Gerar sequências únicas a partir da mensagem original.
     */
    public void getSequenciasUnicas() {
        int tamanhoMensagem = mensagem.getTamanhoMensagem();
        mMapSequenciasUnicas.put(mensagem.getDaPrimeiraPosicao(), 1);
        StringBuilder key = new StringBuilder(mensagem.getDaPosicao(1));
        for (int i = 1; i < tamanhoMensagem; i++) {
            while (contemChave(key.toString())) {
                String auxiliar = mensagem.getDaPosicao(i + 1);
                if (auxiliar != null) {
                    key.append(auxiliar);
                    i++;
                } else {
                    break;
                }
            }

            mMapSequenciasUnicas.put(key.toString(), mMapSequenciasUnicas.size() + 1);
            String auxiliar = mensagem.getDaPosicao(i + 1);
            if (auxiliar != null) {
                key = new StringBuilder(auxiliar);
            } 
        }
        preparaDicionario();
    }

    /**
     *
     * Gera o dicionario de huffman a partir do mapa de sequências únicas.
     */
    private void preparaDicionario() {
        Set<String> mSet = mMapSequenciasUnicas.keySet();
        mDicionario = new String[mMapSequenciasUnicas.size()];
        String subString;
        for (String mSeq : mSet) {
            if (mSeq.length() == 1) {
                mDicionario[mMapSequenciasUnicas.get(mSeq) - 1] = 0 + caracterDeEscape + mSeq;
            } else {
                subString = mSeq.substring(0, mSeq.length() - 1);
                if (mMapSequenciasUnicas.containsKey(subString)) {
                    mDicionario[mMapSequenciasUnicas.get(mSeq) - 1] = mMapSequenciasUnicas.get(subString) + caracterDeEscape + mSeq.charAt(mSeq.length() - 1);
                }
            }
        }
    }

    private boolean contemChave(String key) {
        return mMapSequenciasUnicas.containsKey(key);
    }

    /**
     * @return String decodificada.
     * @param dicionario recebido para ser decodificado.
     * Decodifica a mensagem original a partir do dicionário recebido.
     */
    public String decodificacao(String[] dicionario) {
        StringBuilder mensagemDecodificada = new StringBuilder();
        ArrayList<String> sequenciasRecebidas = new ArrayList<>();
        String[] aux;
        for (String md : dicionario) {
            if (md.charAt(0) == '0') {
                if (md.length() < 3) {
                    sequenciasRecebidas.add(" ");
                } else {
                    sequenciasRecebidas.add(md.charAt(md.length() - 1) + "");
                }

            } else {
                aux = md.split(caracterDeEscape);
                if (aux.length < 2) {
                    sequenciasRecebidas.add(sequenciasRecebidas.get(Integer.parseInt(aux[0]) - 1) + " ");
                } else {
                    sequenciasRecebidas.add(sequenciasRecebidas.get(Integer.parseInt(aux[0]) - 1) + md.charAt(md.length() - 1));
                }
            }
        }
        for (String aux1 : sequenciasRecebidas) {
            mensagemDecodificada.append(aux1);
        }

        return mensagemDecodificada.toString();
    }

    public double getTaxaCompressao() {
        int quantidadeBitsMensagem = mensagem.getTamanhoMensagem() * 8;
        int quantidadeBitsDicionario = mDicionario.length * getNumeroBits() + mDicionario.length * 8;

        return 1 - quantidadeBitsDicionario / (float) quantidadeBitsMensagem;
    }

    public String getTaxaCompressaoPorcentagem() {
        int quantidadeBitsMensagem = mensagem.getTamanhoMensagem() * 8;
        int quantidadeBitsDicionario = mDicionario.length * getNumeroBits() + mDicionario.length * 8;
        return NumberFormat.getPercentInstance(Locale.ENGLISH).format(1 - quantidadeBitsDicionario / (double) quantidadeBitsMensagem);
    }

}
