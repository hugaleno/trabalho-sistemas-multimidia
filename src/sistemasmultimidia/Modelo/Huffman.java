/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

/**
 *
 * @author Hugaleno
 */
public class Huffman {
    
 
    public Huffman() {
        
    }  
    /**
     *
     * @param simbolos Sequencia de simbolos 
     * @return Um Arraylist com todos os simbolos e seus respectivos códigos de Huffman.
     */
    public ArrayList<Simbol> gerarHuffmanCode(ArrayList<Simbol> simbolos) {
        Simbol menor1, menor2, aux;
        LinkedList<Simbol> linkedList = new LinkedList<>(simbolos);
      
        ArrayList<Simbol> simbolosHuffman = new ArrayList<>();
        while (simbolos.size() > 1) {
            menor1 = simbolos.remove(0);
            menor2 = simbolos.remove(0);
            menor1.addCodigo(0);
            menor2.addCodigo(1);
            if(menor1.getCodigo()!=null){
                simbolosHuffman.add(menor1);
            }
            if(menor2.getCodigo()!= null){
                simbolosHuffman.add(menor2);
            }
            
            aux = new Simbol(menor1.getNome() + menor2.getNome());
            Simbol[] simb = new Simbol[]{menor1, menor2};
            aux.setFrequencia(menor1.getFrequencia() + menor2.getFrequencia());
            aux.addSimboloComposto(simb);
            simbolos.add(aux);
            Collections.sort(simbolos);
        }
        return simbolosHuffman;

    }
    
    
}
