/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;

/**
 *
 * @author Hugaleno
 */
public class Simbol implements Comparable<Simbol> {

    private String nome;
    private int ocorrencias;
    private double frequencia;
    private final ArrayList<Integer> codigo;
    private final ArrayList<Simbol> simboloComposto;

    /**
     *
     * @param nome Cria um novo simbolo com o nome recebido.
     */
    public Simbol(String nome) {
        this.nome = nome;
        this.ocorrencias = 0;
        this.frequencia = 0;
        this.codigo = new ArrayList<>();
        this.simboloComposto = new ArrayList<>();
    }

    /**
     * Cria um novo objeto a partir dos parâmentros recebidos.
     *
     * @param nome Nome do simbolo
     * @param ocorrencias O número de ocorrencias do simbolo.
     * @param frequencia Frequência do simbolo.
     */
    public Simbol(String nome, int ocorrencias, double frequencia) {
        this.nome = nome;
        this.ocorrencias = ocorrencias;
        this.frequencia = frequencia;
        this.codigo = new ArrayList<>();
        this.simboloComposto = new ArrayList<>();
    }

    /**
     *
     * @return String com o nome do simbolo
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome Altera o nome do símbolo de acordo com o valor recebido.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return Retorna um inteiro com o número de ocorrências.
     */
    public int getOcorrencias() {
        return ocorrencias;
    }

    /**
     *
     * @param ocorrencias Seta o número de ocorrências de acordo com o valor
     * recebido.
     */
    public void setOcorrencias(int ocorrencias) {
        this.ocorrencias = ocorrencias;
    }

    /**
     *
     * @return Retorna um double com o valor da frequência daquele símbolo.
     */
    public double getFrequencia() {
        return frequencia;
    }

    /**
     *
     * @return Retorna uma string que representa o código referente a este
     * símbolo ou nulo caso o código tenha tamanho nulo.
     */
    public String getCodigo() {
        String str = codigo.toString().replaceAll(",", "").replaceAll("\\[", "").replaceAll("\\]", "").replaceAll(" ", "");
        StringBuffer strBuffer;
        if (str.isEmpty()) {
            return null;
        } else {
            strBuffer = new StringBuffer(str);
            return strBuffer.reverse().toString().trim();
        }
    }

    /**
     *
     * @param cod Adiciona o valor obtido ao código atual.
     */
    public void addCodigo(Integer cod) {
        if (simboloComposto.size() > 0) {
            for (Simbol simb : simboloComposto) {
                simb.addCodigo(cod);
            }
        } else {
            codigo.add(cod);
        }
    }

    /**
     *
     * @param simb Adiciona os símbolos formadores desse símbolo a um array.
     */
    public void addSimboloComposto(Simbol[] simb) {
        for (int i = 0; i < simb.length; i++) {
            simboloComposto.add(simb[i]);
        }

    }

    public static ArrayList<Simbol> criaSimbolosFromArray(ArrayList<String> nomes) {
        int cont=0;
        ArrayList<Simbol> simbolos = new ArrayList<>();
        HashSet<String> simbolosUnicos = new HashSet<>();
        for (String n : nomes) {
            if(simbolosUnicos.add(n)){
                simbolos.add(new Simbol(n.trim()));
            }
        }
        
        for (Simbol simb : simbolos) {
            for (String nome : nomes) {
                if(simb.getNome().equals(nome)){
                    cont++;
                }
            }
            simb.setOcorrencias(cont);
            simb.setFrequencia(cont/(double) nomes.size());
            cont = 0;
        }
        return simbolos;
    }

    /**
     *
     * @param frequencia Seta a frequência desse simbolo de acordo com o valor
     * recebido.
     */
    public void setFrequencia(double frequencia) {
        DecimalFormat df = new DecimalFormat("0.###");

        this.frequencia = Double.parseDouble(df.format(frequencia).replace(",", "."));
    }

    @Override
    public int compareTo(Simbol o) {
        if (this.frequencia < o.getFrequencia()) {
            return -1;
        }
        if (this.frequencia > o.getFrequencia()) {
            return 1;
        }
        return 0;
    }
}
