/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemasmultimidia.Controle;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import sistemasmultimidia.Modelo.Gerenciador;
import sistemasmultimidia.Modelo.Huffman;
import sistemasmultimidia.Modelo.LZ78;
import sistemasmultimidia.Modelo.Simbol;
import sistemasmultimidia.Visao.Comunicacao;
import sistemasmultimidia.Visao.CriarArquivo;
import sistemasmultimidia.Visao.Inicio;

/**
 *
 * @author Hugaleno, Stéphanie
 */
public class Controller {

    private final Inicio inicio;
    private Comunicacao comunicacao;
    private CriarArquivo criarArquivo;
    private final Gerenciador gerente;
    private LZ78 mLZ78;
    private String msgCodificadaSequencia, msgCodificadaCaracter;
    private HashMap<String, String> mMapSequenciaCodigo, mMapCaracteresCodigo;
    private HashMap<String, String> mMapCodigoSequencia, mMapCodigoCaracteres;
    private ArrayList<Simbol> simbolos;
    private ArrayList<Simbol> simbolosSequencia;
    private ArrayList<Simbol> simbolosSequenciaCodificados;
    private ArrayList<Simbol> simbolosCaracteres;
    private ArrayList<Simbol> simbolosCaracteresCodificados;
    private ArrayList<String> sequenciaDecodificada;
    private ArrayList<String> caracteresDecodificados;
    private String[] dicionarioDecodificado;
    private String caracteresDesejados;
    public static int ID_MSG = 0;

    /**
     *
     * @param inicio Uma instância da classe Inicio
     */
    public Controller(Inicio inicio) {
        this.inicio = inicio;
        inicio.addButtonsAction(new InicioActionsPerformed());
        inicio.addMenuItensActions(new JMenuItemActionsPerformed());
        inicio.setVisible(true);
        simbolos = new ArrayList<>();
        gerente = new Gerenciador();

    }

    private class JMenuItemActionsPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == inicio.getMnuItemCriarArquivo()) {
                criarArquivo = new CriarArquivo(inicio, true);
                criarArquivo.addButtonsActionListener(new CriarArquivoButtonsActionPerformed());
                criarArquivo.setVisible(true);

            }
        }

    }

    private class CriarArquivoButtonsActionPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == criarArquivo.getBtnSelecionarDiretorio()) {
                JFileChooser selecionarDiretorio = new JFileChooser();
                selecionarDiretorio.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                File file = new File("C:/Users/Hugaleno/Documents/NetBeansProjects/SistemasMultimidia/");
                selecionarDiretorio.setSelectedFile(file);
                int returnVal = selecionarDiretorio.showOpenDialog(inicio);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    criarArquivo.setDiretorioSelecionado(selecionarDiretorio.getSelectedFile().getPath());
                }
            } else if (e.getSource() == criarArquivo.getBtnCriarArquivo()) {
                int tam = criarArquivo.getQuantidadeSelecionada();
                caracteresDesejados = criarArquivo.getCaracteresDesejados();
                try {
                    FileWriter arq = new FileWriter(inverteBarraCaminho(criarArquivo.getDiretorioSelecionado()) + "texto.txt");
                    PrintWriter gravarArq = new PrintWriter(arq);
                    for (int i = 0; i < tam; i++) {
                        gravarArq.print(criaCaractere());
                        criarArquivo.setProgressBar(i);
                    }

                    arq.close();
                    
                    JOptionPane.showMessageDialog(criarArquivo, "Arquivo com o nome texto.txt criado com sucesso no diretório selecionado.", "Mensagem", JOptionPane.INFORMATION_MESSAGE);
                } catch (IOException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    private char criaCaractere() {
        Random rnd = new Random();
        return caracteresDesejados.charAt(rnd.nextInt(caracteresDesejados.length()));
    }

    private class InicioActionsPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == inicio.getBtnProcurarArquivo()) {
                JFileChooser procurar = new JFileChooser();
                File file2 = new File("D:/Documents/NetBeansProjects/SistemasMultimidia/mensagemOriginal.txt");
                File file = new File("D:/Documents/NetBeansProjects/SistemasMultimidia");
                procurar.setSelectedFile(file2);
                procurar.setCurrentDirectory(file);

                int returnVal = procurar.showOpenDialog(inicio);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    inicio.setNomeArquivo(procurar.getSelectedFile().getName());

                    /**
                     * Ler arquivo.
                     */
                    simbolos = gerente.lerArquivo(procurar.getSelectedFile().getPath());

                    mLZ78 = new LZ78(gerente.getMensagem());
                    mLZ78.getSequenciasUnicas();
                    simbolosSequencia = Simbol.criaSimbolosFromArray(mLZ78.getNumSequenciaUnicas());
                    simbolosCaracteres = Simbol.criaSimbolosFromArray(mLZ78.getCaracteres());

                    Collections.sort(simbolos);
                    inicio.setEntropia(gerente.calculaEntropia(simbolos));
                    Huffman huffman = new Huffman();
                    ArrayList<Simbol> array = huffman.gerarHuffmanCode(simbolos);
                    Collections.sort(simbolosSequencia);
                    simbolosSequenciaCodificados = huffman.gerarHuffmanCode(simbolosSequencia);
                    Collections.sort(simbolosCaracteres);
                    simbolosCaracteresCodificados = huffman.gerarHuffmanCode(simbolosCaracteres);

                    /**
                     * Preenche tabela.
                     */
                    for (Simbol simbolo : array) {
                        if (simbolo.getCodigo() != null) {
                            inicio.preencheTabela(new Object[]{simbolo.getNome(), simbolo.getCodigo(), simbolo.getFrequencia(), simbolo.getOcorrencias()});
                        }

                    }

                }
            } else if (e.getSource() == inicio.getBtnComunicacao()) {
                if (comunicacao == null) {
                    comunicacao = new Comunicacao();
                    comunicacao.addButtonsActionListener(new ComunicacaoActionsPerformed());
                    comunicacao.setVisible(true);
                    comunicacao.setMensagemASerEnviada(mLZ78.getMensagem().toString());
                    atualizaTabelaSequenciaCodificados();
                    atualizaTabelaCaracteresCodificados();
                    atualizaTabelaDicionario();
                } else {
                    if (!comunicacao.isVisible()) {
                        comunicacao.setVisible(true);
                    }
                    comunicacao.requestFocus();
                }

            }

        }
    }

    private class ComunicacaoActionsPerformed implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == comunicacao.getBtnCodificar()) {
                codificaSequenciasECaracteres();
                comunicacao.setSequenciaCodificada(msgCodificadaSequencia);
                comunicacao.setCaracteresCodificados(msgCodificadaCaracter);
                
            } else if (e.getSource() == comunicacao.getBtnEnviar()) {
                comunicacao.setSequenciaRecebida(comunicacao.getSequenciaTransmissao());
                comunicacao.setCaracteresRecebidos(comunicacao.getCaracteresTransmissao());
                comunicacao.setTaxaCompressao(mLZ78.getTaxaCompressaoPorcentagem());
                
            } else if (e.getSource() == comunicacao.getBtnDecodificarDicionario()) {
                decodificarSequenciaRecebida();
                decodificarCaracteresRecebidos();
                preencheDicionarioDecodificado();
                
            } else if (e.getSource() == comunicacao.getBtnDecodificarMensagem()) {
                comunicacao.setMensagemDecodificada(mLZ78.decodificacao(dicionarioDecodificado).replace("§", ""));
            }
        }

    }

    private void preencheDicionarioDecodificado() {
        dicionarioDecodificado = new String[sequenciaDecodificada.size()];
        for (int i = 0; i < sequenciaDecodificada.size(); i++) {
            dicionarioDecodificado[i] = sequenciaDecodificada.get(i) + mLZ78.getCaracterDeEscape() + caracteresDecodificados.get(i);
            comunicacao.preecheTabelaDicionarioDecodificado(new Object[]{sequenciaDecodificada.get(i),
                caracteresDecodificados.get(i)});
        }

    }

    private void decodificarSequenciaRecebida() {
        String sequenciaCodificada = comunicacao.getSequenciaCodificada();
        StringBuilder key = new StringBuilder();
        sequenciaDecodificada = new ArrayList<>();
        for (int i = 0; i < sequenciaCodificada.length(); i++) {
            key.append(sequenciaCodificada.charAt(i));
            if (mMapCodigoSequencia.containsKey(key.toString())) {
                sequenciaDecodificada.add(mMapCodigoSequencia.get(key.toString()));
                key = new StringBuilder("");
            }
        }
    }

    private void decodificarCaracteresRecebidos() {
        String caracteresCodificados = comunicacao.getCaracteresCodificados();
        StringBuilder key = new StringBuilder();
        caracteresDecodificados = new ArrayList<>();
        for (int i = 0; i < caracteresCodificados.length(); i++) {
            key.append(caracteresCodificados.charAt(i));
            if (mMapCodigoCaracteres.containsKey(key.toString())) {
                caracteresDecodificados.add(mMapCodigoCaracteres.get(key.toString()));
                key = new StringBuilder("");
            }
        }
    }

    private void codificaSequenciasECaracteres() {
        ArrayList<String> simbolosSequenciaNomes = mLZ78.getNumSequenciaUnicas();
        StringBuilder msgCodificada = new StringBuilder("");
        for (String nomeSimb : simbolosSequenciaNomes) {
            if (mMapSequenciaCodigo.containsKey(nomeSimb)) {
                msgCodificada.append(mMapSequenciaCodigo.get(nomeSimb));
            }
        }
        ArrayList<String> simbolosCaracteresNomes = mLZ78.getCaracteres();
        msgCodificadaSequencia = msgCodificada.toString();
        msgCodificada = new StringBuilder("");
        for (String nomeSimb : simbolosCaracteresNomes) {

            if (nomeSimb.equals(" ")) {
                msgCodificada.append(mMapCaracteresCodigo.get(""));
            } else {
                msgCodificada.append(mMapCaracteresCodigo.get(nomeSimb));
            }

        }
        msgCodificadaCaracter = msgCodificada.toString();
    }

    private void atualizaTabelaSequenciaCodificados() {
        mMapSequenciaCodigo = new HashMap<>();
        mMapCodigoSequencia = new HashMap<>();

        for (Simbol simb : simbolosSequenciaCodificados) {
            if (simb.getCodigo() != null) {
                comunicacao.addTblSequenciasCodificadas(new Object[]{simb.getNome(), simb.getCodigo()});
                geraMapSequencias(simb.getNome(), simb.getCodigo());
            }
        }
    }

    private void geraMapSequencias(String nome, String codigo) {
        mMapSequenciaCodigo.put(nome, codigo);
        mMapCodigoSequencia.put(codigo, nome);
    }

    private void atualizaTabelaCaracteresCodificados() {
        mMapCaracteresCodigo = new HashMap<>();
        mMapCodigoCaracteres = new HashMap<>();
        for (Simbol simb : simbolosCaracteresCodificados) {
            if (simb.getCodigo() != null) {
                comunicacao.addTblCaracteresCodificados(new Object[]{simb.getNome(), simb.getCodigo()});
                geraMapCaracteres(simb.getNome(), simb.getCodigo());
            }
        }
    }

    private void geraMapCaracteres(String nome, String codigo) {
        mMapCaracteresCodigo.put(nome, codigo);
        mMapCodigoCaracteres.put(codigo, nome);
    }

    private void atualizaTabelaDicionario() {
        String[] dicionario = mLZ78.getmDicionario();
        String[] aux;
        for (String simb : dicionario) {
            aux = simb.split(mLZ78.getCaracterDeEscape());
            comunicacao.addTblDicionario(new Object[]{aux[0], aux[1]});

        }
    }

    protected String inverteBarraCaminho(String caminho) {
        return caminho.replaceAll("\\u005c", "/") + "/";
    }
}
